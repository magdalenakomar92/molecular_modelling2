import random as rnd
import numpy as np
import matplotlib.pyplot as pp

m1 = 1
no_steps = 1000

# setting intitial position and speed
x0 = np.array([0, 0])
v0 = np.array([0, 0])

xi = x0
vi = v0

# setting time-step
ti = 0.01
x_list = np.empty((no_steps + 1, 2))
v_list = np.empty((no_steps + 1, 2))

x_list[0, :] = xi
v_list[0, :] = vi


def force():
    x = rnd.uniform(-1.0, 1.0)
    y = rnd.uniform(-1.0, 1.0)
    l = (x ** 2 + y ** 2) ** 0.5
    return np.array([x / l, y / l])


# leapfrog algorithm:
def leapfrog(no_steps, vi, xi):
    for i in range(no_steps):
        ai = force() / m1
        vi_12 = vi + ti * ai

        xi_1 = xi + vi_12 * ti

        x_list[i + 1, :] = xi_1
        v_list[i + 1, :] = vi_12

        vi, xi = vi_12, xi_1


def main():
    leapfrog(no_steps, vi, xi)

    pp.plot(x_list[:, 0], x_list[:, 1])
    pp.show()

    d = ((xi[0] - x0[0]) ** 2 + (xi[1] - x0[1]) ** 2) ** 0.5
    print(f'distance from ending to initial position is {d}')
    return 0


if __name__ == '__main__':
    main()
